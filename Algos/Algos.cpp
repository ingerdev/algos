// Algos.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <vector>
#include <array>

#include "../SortingAlgos/SortCase.h"
#include "../SortingAlgos/SortDefaults.h"
#include "../SortingAlgos/SortMeasurements.h"
#include "../SortingAlgos/SortResultReports.h"
#include "MeasureAlgo.h"

#include "../SortingAlgos/MergeSort.h"
#include "../SortingAlgos/InsertionSort.h"
#include "../SortingAlgos/BubbleSort.h"
#include "../SortingAlgos/ShellSort.h"
#include "../SortingAlgos/QuickSort.h"
#include "../SortingAlgos/CountSort.h"
#include "../SortingAlgos/RadixSort.h"

#include "../GraphAlgos/ListGraph.h"


template<class It>
const char* mysort(It b, It e)
{
	std::sort(b, e);
	return "freefunc";
}

template <typename It>
class call_me
{
public:
	const char* operator()(It b, It e)
	{
		std::sort(b, e);
		return "functor";
	}
};


template <typename Cont/*,typename It = Cont::iterator, typename F  = const char * (It, It)*/>
class algos_holder
{
public:	
	typedef Cont container_type;
	using writer_function_type =  std::function<const char *(typename Cont::iterator,typename Cont::iterator)>;
	
	template <typename F>
	void add_algo(F && algo)
	{
		algos_.push_back(writer_function_type(algo));
	}

	const std::vector<writer_function_type> get_algos() const
	{
		return algos_;
	}

private:
	std::vector<writer_function_type> algos_;
};



using namespace sort_space;
void test_sort()
{
	std::cout << sizeof(uint32_t);
	auto sort_func = [](auto b, auto e) -> const char* {
		std::sort(b, e);
		return "std::sort";

	};

	auto sort_stable_func = [](auto b, auto e) -> const char* {
		std::stable_sort(b, e);
		return "std::stable_sort";

	};

	auto case_generator = make_default_sort_cases<int>(1, 200);
	//make_default_sort_cases<int>();

	using value_type = decltype(case_generator)::value_type;
	using Cont = std::vector<value_type>;

	algos_holder<Cont> algos;

	//algos.add_algo(call_me<Cont::iterator>());		
	//algos.add_algo(mysort<Cont::iterator>);

	algos.add_algo(sort_space::quick_sort::sort<Cont::iterator>);
	algos.add_algo(sort_func);
	algos.add_algo(sort_stable_func);
	algos.add_algo(sort_space::merge_sort::sort<Cont::iterator>);
	algos.add_algo(sort_space::insertion_sort::sort<Cont::iterator>);

	//algos.add_algo(sort_space::insertion_sort::naive_sort<Cont::iterator>);		
	//algos.add_algo(sort_space::bubble_sort::naive_sort<Cont::iterator>);

	algos.add_algo(sort_space::shell_sort::sort<Cont::iterator>);
	algos.add_algo(sort_space::count_sort::sort<Cont::iterator>);
	algos.add_algo(sort_space::radix_sort::sort<3, Cont::iterator>);
	algos.add_algo(sort_space::radix_sort::sort<4, Cont::iterator>);
	algos.add_algo(sort_space::radix_sort::sort<2, Cont::iterator>);
	algos.add_algo(sort_space::radix_sort::sort<8, Cont::iterator>);

	SortMeasureResultsStorage<value_type> measurements_storage;

	measurement::measure_algos(case_generator, algos.get_algos(),
		sort_space::measure_loop_execution_time<Cont::iterator>, measurements_storage);

	auto cout_stream_ref = &std::cout;
	report_measures_by_default(cout_stream_ref, measurements_storage.get_results());
}
void test_graph()
{
	using namespace graph_space;
	using graph_type = ListGraph<Node>;
	graph_type graph;
	graph.add_node({ "A",2 });
	//static_assert(std::is_same_v<graph_type::element_type::edge_type, Edge>,"Types mismatch");
}

struct c
{
	std::array<int, 1000 * 1000 * 100> x;
};
int main()
{
	test_sort();
	c p;
	//test_graph();
	return 0;
}

