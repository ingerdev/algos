#pragma once
#include <utility>
#include <chrono>
#include <string>
#include "../SortingAlgos/SortMeasurements.h"

namespace measurement
{
	
	template <typename G, //test data generator
			typename U, //algos-holding container	
			typename M, //algo exceution time measurer
			typename S //algos measure results storage
	>
	void measure_algos(G test_data_generator,U test_algos,M algo_measurer,S& results_storage)	
	{
		using T = typename G::value_type;
		for (const auto& test_case:test_data_generator.cases())
		{
			//generate test case data from description				
			auto test_data = generate_case_data(test_case);

			//use every algos on test case data 
			for (const auto& algo : test_algos)
			{
				//make data copy
				auto data_copy(test_data);

				//returns std::pair<algo_name,algo_exec_time>
				auto measured_algo = algo_measurer(test_case.repeat_cycles, test_data.begin(), test_data.end(), algo);
				
				std::string measured_algo_name = measured_algo.first;
				auto measured_algo_exec_duration = measured_algo.second;

				results_storage.save_measure(test_case,measured_algo_name, measured_algo_exec_duration);
			}
		}
	}
}
