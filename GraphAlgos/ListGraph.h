#pragma once
#include <vector>
#include <unordered_map>
#include <memory>
#include <functional>

//graph as adjacency vertex linked list
namespace graph_space
{
	
	struct Node
	{
		std::string name;
		int value;		
		bool operator==(const Node&rhs) { return name == rhs.name; }
	};

	struct NodeHasher
	{
		std::size_t operator()(const Node& node) const
		{
			return std::hash<std::string>{}(node.name);
		}
		std::size_t operator()(const Node* node) const
		{
			return std::hash<std::string>{}(node->name);
		}
		
		std::size_t operator()(const std::unique_ptr<Node>& node) const
		{
			return std::hash<std::string>{}(node->name);
		}
	};

	struct Edge
	{
		int value;
	};

	template <typename V, typename E>
	struct ListElement
	{
		using node_type = V;
		using edge_type = E;

		V* node;
		E* edge;
		ListElement * next_element;
		
	};

	/*
	template <typename V>
	struct ListElement<V,std::nullptr_t>
	{
		using node_type = V;

		V* node;
		ListElement * next_element;
	};
	*/

	template <typename V, typename E = std::nullptr_t>
	class ListGraph
	{		
	public:
		using node_type = V;
		using node_ptr = node_type * ;
		using unique_node_ptr = std::unique_ptr<node_type>;

		using edge_type = E;
		using unique_edge_ptr = std::unique_ptr<edge_type>;
		
		using element_type = ListElement<V, E>;		
		using element_ptr = element_type*;
		using unique_element_ptr = std::unique_ptr<element_type> ;

		using nodes_list_type = std::list<unique_node_ptr>;
		using adjacent_map_type = std::unordered_map<node_ptr, element_ptr,NodeHasher>;

		ListGraph(){}
		~ListGraph(){}

		void add_edge(node_ptr a, node_ptr b)
		{
			//if 
		}

		void add_node(node_type new_node)
		{
			
			//throw in node with this name already present in nodes_			
			if (node_already_exist(nodes_, &new_node))
				throw std::logic_error("node with name \"" + new_node.name +
					"\" already present in nodes list");
					
			//throw if this element already present in the map at root level
			if (node_already_exist(adjacent_map_, &new_node))
				throw std::logic_error("node key with name \"" + new_node.name +
					"\" already present in adjacent map");
					
			//register node in nodelist			
			auto u_node = std::make_unique<node_type>(new_node);			
			nodes_.push_back(std::move(u_node));									
		}		

		void add_edge(node_ptr source_node, element_type edge)
		{
			auto& target_node = edge.node;
			//check node exist		
			//throw if this element already present in the map at root level
			if (!node_already_exist(adjacent_map_, &source_node))
				throw std::logic_error("node key with name \"" + source_node.name +
					"\" does not present in adjacent map");

			//throw if destination element not present in the map at root level
			if (!node_already_exist(adjacent_map_, &target_node))
				throw std::logic_error("node key with name \"" + target_node.name +
					"\" does not present in adjacent map");
			
			auto u_edge = std::make_unique<element_type>(edge);
			auto element = u_edge.get();

			//register edge in adjacent map
			//check source_node has at least single registered edge
			auto first_element = adjacent_map_.find(source_node);
			if (first_element == adjacent_map_.end())
			{
				adjacent_map_[source_node] = std::move(u_edge);
				return;
			}

			//element list exists. add to the end of list
			auto current_element = *first_element;//adjacent_map_[source_node];
			while (current_element->next_element != nullptr)
			{
				if (current_element->node == target_node)
					throw std::logic_error("element with name \"" + edge.node->name +
						"\"already presents in adjacent map of root node \"" + adjacent_map_[source_node]->node->name + "\"");

				current_element = current_element->next_element;
			}
			current_element->next_element = element;
		}

		static bool node_already_exist(const nodes_list_type& nodes,const node_ptr node)
		{
			return std::any_of(nodes.begin(), nodes.end(),
				[&](const unique_node_ptr& element_node) { return *node == *element_node.get(); });
		}

		static bool node_already_exist(const adjacent_map_type& adjacents, const node_ptr node)
		{			
			return adjacents.find(node)!=adjacents.end();			
		}

		node_ptr find_node(node_ptr input_node)
		{
			for (const auto& node : nodes_)
			{
				if (equal_nodes(input_node, node))
					return node;
			}
			return nodes_.end();
		}
		
	private:

		//add root_element to adjacent_map
		void add_root_element(element_type new_root_element)
		{
			

			auto element_u = std::make_unique<element_type>(new_root_element);
			adjacent_map_[new_root_element.node] = element_u.get();
			
			//hold ownage link
			elements_.push_back(std::move(element_u));
			
		}
		
		//ownership
		std::list<unique_node_ptr> nodes_;
		std::list<unique_edge_ptr> edges_;
		std::list<unique_element_ptr> elements_;

		adjacent_map_type adjacent_map_;
	};
}
