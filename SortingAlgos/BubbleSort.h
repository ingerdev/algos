
namespace sort_space
{

	namespace bubble_sort
	{

		template<class It>
		const char* naive_sort(It b, It e)
		{
			auto length = e-b;
			for (auto k = 0; k!= length-1;++k)
				for (auto k1 = length-1; k1 > k; --k1)
				{
					auto top = b + k1;
					auto sub_top = top - 1;
					if ((*top) < (*sub_top))
						std::swap(*top , *sub_top);
				}
			return "my_naive_bubble_sort";
		}		
	}
}