#include <type_traits>
#include <numeric>
#include <vector>
namespace sort_space
{
	namespace count_sort
	{
		template <typename It>
		const char * sort(It b, It e)
		{
			constexpr size_t bucket_size_bits = 9;

			using value_type = typename It::value_type;
			//static_assert((sizeof(value_type) <= 1) << bucket_size_bits,
			//	"value_type is heavier than expected ");
			static_assert(std::is_integral_v<value_type>,
				"value_type should be integer type");

			//static_assert(std::is_unsigned_v<value_type>,
			//	"value_type should be unsigned type ");

			std::array<size_t, 1 << bucket_size_bits> count_space{ 0 };

			auto original_b(b);
			std::vector<value_type> sorted_data(e - b);
			
			while (b != e)	++count_space[*b++];
			
			size_t total_count = 0;
			for (auto& element : count_space)
			{				
				element += total_count;
				total_count = element;
			}

			auto sorted_begin = sorted_data.begin();
			
			do
			{
				auto index = *(--b);
				sorted_data[count_space[index]-1] = *b;
				--count_space[index];
			} while (b != original_b);

			//copy sorted data back
			std::copy(sorted_data.begin(), sorted_data.end(), b);

			return "my_count_sort";
		}
	}
}