#include <iostream>
namespace sort_space
{

	namespace insertion_sort
	{
		template<class It>
		const char* naive_sort(It b, It e)
		{
			for (auto k = b + 1; k != e; ++k)
			{
				
				auto key = *k;
				auto k1{ k-1 };
				while ((k1 >= b) && (key < *k1))
				{
					*(k1 + 1) = *(k1);
					--k1;
				}

				*(k1 + 1) = key;
			}
			return "my_naive_ins_sort";
		}

		template<class It>
		const char* sort(It b, It e)
		{
			//std::copy(b, e, std::ostream_iterator<typename It::value_type>(std::cout, " "));
			//std::cout << "\n";
			using T = typename It::value_type;
			for (auto k = b + 1; k != e; ++k)
			{
				T key(*k);
				auto k1{ k - 1 };
				while ((key < *k1)&&(k1 != b))
				{
					--k1;
				}
								
				//this element is currently sorted, skip it
				if ((key > *k1) && ((k - k1) == 1))
					continue;
			
				//"shift array one step right				
				auto src_b{ key < *k1 ? k1 : k1 + 1 };
				std::copy_backward(src_b,k, k+1);
				*src_b = key;
				//std::copy(b, e, std::ostream_iterator<typename It::value_type>(std::cout, " "));
				//std::cout << "\n";
			}
			return "my_insertion_sort";
		}
	}
}