#pragma once
#include <ostream>
#include <chrono>
#include <sstream>
#include <iomanip>
#include "SortMeasurements.h"
#include "SortCase.h"

namespace sort_space
{
	std::ostream& operator<<(std::ostream& ostream, const GenEnum& gen)
	{
		switch (gen)
		{
		case GenEnum::random: ostream << "random"; break;
			case GenEnum::reverse_sorted: ostream << "reverse_sorted"; break;
			case GenEnum::sorted: ostream << "sorted"; break;
			case GenEnum::zero_fill: ostream << "zero_fill"; break;
			default: ostream << "unknown fill type"; break;
		}
		return ostream;
	};

	//format and print sorted MeasureResult.
	template <typename T>
	class MeasureResultFormatter
	{
		
	public:
		//ostream where MeasureResults will be printed.
		//notice that ostream formatters will be changed.
		//to restore them call restore_ostream_formatters() method.
		MeasureResultFormatter(std::ostream* output_stream) :ostream_(output_stream)
		{
			fmt_keeper_.copyfmt(*ostream_);
		}
		~MeasureResultFormatter()
		{
			//do not restore ostream_ fmt here
			//let user call reset_ostream_formatters() forcibly.
		}
		
		void restore_ostream_formatters()
		{
			*ostream_.copyfmt(fmt_keeper_);
		}
		void operator<<(MeasureResult<T> value)
		{
			write_measure_result(value);
		}
	private:		
		enum class StateEnum :bool
		{
			undefined, defined
		};

		void write_repeat_cycles(MeasureResult<T> value)
		{
			//if it is the same repeat_cycles value - not print anything
			if (!(state_undefined() || (old_value_.test_case.repeat_cycles !=
				value.test_case.repeat_cycles)))
				return;
			
			//print table header
			using std::setw;
			using std::left;
			using std::internal;
			using std::right;
			
			*ostream_ << "\n-------------------- repeats " << value.test_case.repeat_cycles
				       << " --------------------\n";
			*ostream_ << setw(20) << left     << "algo name " \
			  	      << setw(15) << internal << "N"  \
					  << setw(15) << internal << "fill type" \
					  << setw(15) << right    << "time (ms)\n";
			
			*ostream_ << pad<70>(std::string(""), '-') <<"\n";
		};

		void write_fill_type(MeasureResult<T> value)
		{
			//return if we havent change in fill_type
			if ((state_undefined() || (old_value_.test_case.fill_type ==
				value.test_case.fill_type)))
				return;
			
			*ostream_ << "\n";
		}
		void write_measure_result(MeasureResult<T> value)
		{
			//print table header (if necessary)
			write_repeat_cycles(value);			
			//print empty line between values with different fill_types
			write_fill_type(value);

			using std::setw;
			using std::left;
			using std::internal;

			*ostream_ << setw(20) << left     << value.algo_name << setw(0) << "|" << \
						 setw(15) << internal << value.test_case.elements_count << setw(0) << "|" << \
						 setw(15) << internal << value.test_case.fill_type << setw(0) << "|" << \
						 setw(10) << internal << std::chrono::duration_cast<
												 std::chrono::milliseconds>(value.measure_time).count() \
						 << setw(0) << "\n";
				
			
			old_value_ = value;
			define_state();
		}
		
		template<size_t N = 20,typename C>
		std::basic_string<C> pad(const std::basic_string<C>& s,C c=' ')
		{
			auto result = s;
			if (N > result.length())
				result.append(N - result.length(), c);
			return result;
		}

		bool state_undefined()
		{
			return state_ == StateEnum::undefined;
		}
		void define_state()
		{
			state_ = StateEnum::defined;
		}

		//last value passed trough us
		MeasureResult<T> old_value_;	

		//external-provided ostream to write
		std::ostream* ostream_;
		
		//stream that will keep original ostream_ formatters
		std::stringstream fmt_keeper_;

		//undefined state mean we not yet got first MeasureResult value
		//so cant compare new value with old (as we havent "old" value yet)
		StateEnum state_ = StateEnum::undefined;
	};
}
