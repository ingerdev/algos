#pragma once

namespace sort_space
{

	namespace merge_sort
	{

		template<class It>
		void merge(It left, It right,const It end, It dest_b)
		{		
			auto original_right_iterator(right);

			while (true)
			{
				if ((*left) > (*right))
					*dest_b++ = *right++;
				else
					*dest_b++ = *left++;		

				if (right == end)
				{
					std::copy(left, original_right_iterator, dest_b);
					break;
				}

				if (left == original_right_iterator)
				{
					std::copy(right, end, dest_b);
					break;
				}
			}

			return;
		}

		template<class It>
		void split(const It b, const It e, It dest_b)
		{		
			if (e - b == 1)			
				return;		
			
			size_t split_size{static_cast<size_t>((e - b) / 2) };
			
			split(dest_b, dest_b + split_size, b);			
			split(dest_b + split_size, dest_b + (e - b), b + split_size);	
			
			merge(b, b + split_size, e, dest_b);		
		}

		template<class It>
		const char* sort(It b, It e)
		{
			using value_type = typename It::value_type;
			std::vector<value_type> memory(b,e);

			split(memory.begin(), memory.end(), b);
			return "my_merge_sort";						
		}
	}
}

