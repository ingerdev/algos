namespace sort_space
{
	namespace quick_sort
	{
		template<class It>
		It partition(It b, It e)
		{
			if ((e - b) < 2) return b;
			auto item = b;
			for (;;)
			{ 
				while (((++b < e) && (*b < *item)));
				while (*(--e) > *item) { if (e <= b) break; }
				
				if (b >= e) break;
				
				std::swap(*b, *e);		
			}
						
			std::swap(*(b-1), *item);			
			return b;
		}

		template<class It>
		void qsort(It b, It e)
		{
			if (e - b < 2)
				return;

			auto i = partition(b, e);
			qsort(b, i);
			qsort(i, e);
		}

		template<class It>
		const char* sort(It b, It e)
		{
			qsort(b, e);
			return "my_quick_sort";
		}
	}
}