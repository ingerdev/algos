#include <numeric>
#include <climits>
namespace sort_space
{
	namespace radix_sort
	{
		template <typename It>
		inline auto get_masked_value(It element, size_t bit_bucket, 
			typename It::value_type mask) -> std::decay_t<decltype(mask)>
		{
			return ((*element) >> bit_bucket)&mask;
		}

		template <size_t bits, typename It>
		void count_sort(It b, It e,It sort_b,size_t bit_bucket)
		{
			static_assert(bits < CHAR_BIT * sizeof(unsigned short),
				"1 << bits exceed short type size");
			static_assert(bits > 0, "template argument bits should be greater zero");

			using value_type = typename It::value_type;			
			constexpr value_type mask = value_type{ 0 } |((1 << bits) - 1);
			
			std::array<size_t, 1 << bits> count_space{ 0 };
			
			//count values
			auto original_b(b);
			while(b!=e) 
			{ 
				++count_space[get_masked_value(b++,bit_bucket,mask)];
			}

			//make index shift array
			size_t elems_count = 0;
			for (auto& element : count_space)
			{
				element += elems_count;
				elems_count = element;
			}

			//place values to sorted array
			do
			{				
				auto index = count_space[get_masked_value(--b, bit_bucket, mask)]-- -1;
				*(sort_b + index) = *b;				
			} while (b != original_b);

		}

		template <size_t N> 
		inline const char * description();
		

		template <>
		inline const char * description<2>()
		{
			return "my_radix_sort_2";
		}
		template <>
		inline const char * description<3>()
		{
			return "my_radix_sort_3";
		}
		template <>
		inline const char * description<4>()
		{
			return "my_radix_sort_4";
		}
		template <>
		inline const char * description<5>()
		{
			return "my_radix_sort_5";
		}
		template <>
		inline const char * description<8>()
		{
			return "my_radix_sort_8";
		}
		template <size_t radix_bits,typename It>
		const char * sort(It b, It e)
		{
			using value_type = typename It::value_type;

			std::vector<value_type> sort_data(e - b);
			auto sort_b = sort_data.begin();
			auto sort_e = sort_data.end();

			constexpr size_t bits = radix_bits;
			size_t bit_bucket = 0;
			bool swapped_storage = false;
			
			while ( (1 << bit_bucket) < ((1 << 8) * sizeof(value_type)))
			{
				//each iteration swap arrays sorted <-> unsorted
				if (swapped_storage)
					count_sort<bits>(sort_b, sort_e, b, bit_bucket);					
				else
					count_sort<bits>(b, e, sort_b, bit_bucket);

				swapped_storage = !swapped_storage;
				bit_bucket += bits;
				
			}
			//if last iteration didnt placed sorted data into original array - copy result
			if (swapped_storage)
				std::copy(sort_b, sort_e, b);

			return description<radix_bits>(); //"my_radix_sort";
		}
	}
}