namespace sort_space
{

	namespace shell_sort
	{		
		template<class It>
		const char* sort(It b, It e)
		{
			for (auto d = (e - b) / 2; d!=0 ; d /= 2)
			{
				for (auto left = b+d; left != e; ++left)
				{
					for (auto right = left; right >=b+d; right -= d)
					{
						if (*(right) >= *(right - d))
							break;

						std::swap(*(right), *(right - d));
						
					}
				}
			}
			return "my_shell_sort";
		}
	}
}

