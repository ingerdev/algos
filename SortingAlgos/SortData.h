#pragma once
#include <vector>
#include <limits>
#include <random>
#include <cassert>

namespace sort_space
{
	namespace internal
	{
		//templatized random generator
		//can generate int/real type random numbers.
		template<typename T,
			typename = typename std::enable_if<std::is_arithmetic<T>::value>::type>
		class random_generator {
			using distribution_type = typename std::conditional<
				std::is_integral<T>::value,
				std::uniform_int_distribution<T>,
				std::uniform_real_distribution<T>
			>::type;

			std::default_random_engine engine;
			distribution_type distribution;

		public:
			random_generator(T min, T max) :distribution(min, max)
			{
			}

			auto operator()() -> decltype(distribution(engine))
			{
				return distribution(engine);
			}
		};

		template<typename Container, typename T = typename Container::value_type>
		auto make_generator(Container const&, T min, T max)
			-> typename std::decay<decltype(std::declval<random_generator<T>>())>::type
		{
			return random_generator<T>(min, max);
		};
	}

	//how array values were initialized
	enum class GenEnum:int
	{
		zero_fill, sorted, reverse_sorted, random
	
	};	

	template <typename T>
	static std::vector<T> generate_zero(size_t count)
	{
		return std::vector<T>(count);
	}
	template <typename T>
	static std::vector<T> generate_random(size_t count,
		T min = std::numeric_limits<T>::min,
		T max = std::numeric_limits<T>::max)
	{
		auto temp = std::vector<T>(count);
		std::generate(temp.begin(), temp.end(), internal::make_generator(temp, min, max));
		return temp;
	}
	template <typename T>
	static std::vector<T> generate_sorted(size_t count,
		T min = std::numeric_limits<T>::min,
		T max = std::numeric_limits<T>::max)
	{
		auto temp = generate_random(count,min,max);
		std::sort(temp.begin(),temp.end(), std::less<>());
		return temp;
	}
	template <typename T>
	static std::vector<T> generate_reverse_sorted(size_t count, 
		T min = std::numeric_limits<T>::min,
		T max = std::numeric_limits<T>::max)
	{
		auto temp = generate_random(count, min, max);
		std::sort(temp.begin(),temp.end(), std::greater<>());
		return temp;
	}

	

	template <typename T>
	static std::vector<T> generate(GenEnum gen_type,
		size_t count,
		T min = std::numeric_limits<T>::min,
		T max = std::numeric_limits<T>::max)
	{
		switch (gen_type)
		{
		case GenEnum::zero_fill: return generate_zero<T>(count);
		case GenEnum::sorted: return generate_sorted<T>(count, min, max);
		case GenEnum::reverse_sorted: return generate_reverse_sorted<T>(count, min, max);
		case GenEnum::random: return generate_random<T>(count, min, max);
		}
		throw std::runtime_error("SortTestCase::generate parameter value gen_type is unknown");
	}

	
}


