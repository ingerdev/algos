#pragma once
#include "SortCase.h"
namespace sort_space
{
	//create default sort cases
	template<typename T>
	SortCases<T> make_default_sort_cases(T min=-300,T max = 300)
	{
		SortCases<T> c;
		//default cases random fill
		//small array 
		//c.add_case(make_case<T>(GenEnum::random, internal::CYCLES_COUNT_FEW, internal::ELEMENTS_COUNT_FEW, min, max));
		c.add_case(make_case<T>(GenEnum::random, 1,30, min, max));
		//big array
		c.add_case(make_case<T>(GenEnum::random, internal::CYCLES_COUNT_FEW, internal::ELEMENTS_COUNT_FEW, min, max));

		//good cases: already sorted arrays
		//small array 
		c.add_case(make_case<T>(GenEnum::sorted, internal::CYCLES_COUNT_FEW, internal::ELEMENTS_COUNT_FEW, min, max));
		//big array 
		//c.add_case(make_case<T>(GenEnum::sorted, internal::CYCLES_COUNT_FEW, internal::ELEMENTS_COUNT_LOTS, min, max));

		//worst case: reverse sorted array
		//small array 
		c.add_case(make_case<T>(GenEnum::reverse_sorted, internal::CYCLES_COUNT_FEW, internal::ELEMENTS_COUNT_FEW, min, max));
		//big array 
		//c.add_case(make_case<T>(GenEnum::reverse_sorted, internal::CYCLES_COUNT_FEW, internal::ELEMENTS_COUNT_LOTS, min, max));

		//sort zero-filled array
		c.add_case(make_case<T>(GenEnum::zero_fill, internal::CYCLES_COUNT_FEW, internal::ELEMENTS_COUNT_FEW, min, max));

		//special cases for small aray but very big cycles
		c.add_case(make_case<T>(GenEnum::reverse_sorted, internal::CYCLES_COUNT_LOTS, 5, min, max));
		c.add_case(make_case<T>(GenEnum::sorted, internal::CYCLES_COUNT_LOTS, 5, min, max));
		c.add_case(make_case<T>(GenEnum::random, internal::CYCLES_COUNT_LOTS, 5, min, max));
		c.add_case(make_case<T>(GenEnum::zero_fill, internal::CYCLES_COUNT_LOTS, 5, min, max));
		return c;
	}
}