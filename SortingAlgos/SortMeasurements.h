#pragma once
#include <algorithm>
#include <chrono>
#include <string>
#include <utility>
#include <variant>

#include "SortCase.h"

namespace sort_space
{
	
	typedef std::pair<std::string, std::chrono::duration<double>> algo_measurement_result;

	//check algo sort equal std::sort sort
	template <typename It, typename Functor = const char*(It, It)>
	bool validate_algo(It begin, It end, std::function<Functor> sort_functor)
	{
		typedef typename std::iterator_traits<It>::value_type value_type;
		const std::vector<value_type> data(begin, end);
		
		std::invoke(sort_functor, data.begin(), data.end());
		return std::is_sorted(data.begin(), data.end());
	}	
	//It - iterators of the container to be sorted.
	template <typename It, typename Functor = const char*(It, It)>
	algo_measurement_result measure_loop_execution_time(size_t iterations_count,
		const It begin, const It end, std::function<Functor> sort_functor)
	{		
		
		//save original data
		typedef typename std::iterator_traits<It>::value_type value_type;
		const std::vector<value_type> unsorted_data(begin, end);

		assert(validate_algo(begin, end, sort_functor));

		

		//loop and sum sort_functor execution time values
		std::chrono::duration<double> time_total(0.0);
		const char * algo_name = nullptr;
		for (size_t k = 0; k < iterations_count; k++)
		{
			//copy original data to new array and run sort over it.
			std::vector<value_type> data(unsorted_data);
			auto t1 = std::chrono::high_resolution_clock::now();
			
			// sort function/functor/lambda return its name as a call result
			algo_name = std::invoke(sort_functor,std::begin(data), std::end(data));
			
			auto t2 = std::chrono::high_resolution_clock::now();
			time_total += (t2 - t1);
		}

		return make_pair(algo_name,time_total);
	}


	template <typename T>
	struct MeasureResult
	{
		sort_case<T> test_case;
		std::string algo_name;
		std::chrono::duration<double> measure_time;
	};

	template <typename T>
	MeasureResult<T> make_measure_result(const sort_case<T> test_case, const std::string& algo_name,
		std::chrono::duration<double> measure_time)
	{
		return { test_case,algo_name,measure_time };
	}

	template <typename T>
	class SortMeasureResultsStorage
	{
	public:
		void clear()
		{
			results_.clear();
		}

		const std::vector<MeasureResult<T>>& get_results()
		{
			return results_;
		}

		void save_measure(const sort_case<T> test_case, const std::string& algo_name,
			std::chrono::duration<double> measure_time)
		{
			results_.push_back(make_measure_result(test_case, algo_name, measure_time));
		}
		
	private:
		//results storage
		std::vector<MeasureResult<T>> results_;
	};



	
}


