#pragma once
#include <ostream>
#include <vector>
#include <numeric>
#include "SortMeasurements.h"
#include "MeasureResultFormatter.h"
namespace sort_space
{
	template <typename T>
	void report_measures_by_default(std::ostream* output_stream,const std::vector<MeasureResult<T>>& measure_results)
	{
		using measure_container_type = std::decay_t<decltype(measure_results)>;
		using measure_index_type = typename measure_container_type::const_iterator;
		
		
		//generate index array (fill index array with iterators to each measure_results element)
		std::vector<measure_index_type> indexes(measure_results.size());
		std::iota(indexes.begin(), indexes.end(), measure_results.cbegin());
		
		//sort from bigger to smaller based on MeasureResult.test_case.repeat_cycles value
		std::sort(indexes.begin(), indexes.end(),
			[](auto left, auto right)
		{
			return default_comparision(*left, *right);
		});
		
		MeasureResultFormatter<T> formatter(output_stream);
		for (auto &index : indexes)
		{
			auto& value = (*index);
			formatter << value;
		}
	}	

	//output writers
	template<typename T>
	void write_repeats_count(std::ostream* output_stream,T repeat_counts)
	{
		*output_stream << "--------------------------- repeats " << repeat_counts << " ------------------------------\n";
	}

	template <typename T>
	bool default_comparision(const MeasureResult<T> left, const MeasureResult<T> right)
	{
		if (left.test_case.repeat_cycles > right.test_case.repeat_cycles) return true;
		if (left.test_case.repeat_cycles < right.test_case.repeat_cycles) return false;

		if (left.test_case.elements_count > right.test_case.elements_count) return true;
		if (left.test_case.elements_count < right.test_case.elements_count) return false;

		if (left.test_case.fill_type < right.test_case.fill_type) return true;
		if (left.test_case.fill_type > right.test_case.fill_type) return false;

		if (left.measure_time < right.measure_time) return true;
		if (left.measure_time > right.measure_time) return false;

		return false;
	}

	
}
